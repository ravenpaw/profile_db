use std::io;
use std::io::Write;
use std::error::Error;
use std::io::prelude::*;
use std::fs::File;
use std::path::Path;
//use std::fs;
//use std::process;

enum InputResult {
    S(String)
    B(bool)
    C(char)
    U8(u8)
    U64(u64)
}

fn get_input(type) -> InputResult {

    //String
    if type = "S" {
        let mut input = String::new();
        io::stdin().read_line(&mut input)
            .expect("Failed to read user input.");

        let mut input: String = match input.trim().parse() {
            Ok(input) => input,
            Err(_) => panic!("Invalid input."), //I don't konw what the fuck to do here or how this works so just panic
        };

        return input;
    }

}

fn main() {

    //Components of the struct for a db entry
    struct Profile {
        name: String,
        age: u8,
        sex: char,
        job: String,
        income: u64,
    }

    //Just a test profile.
    let mut aiden = Profile {
        name: String::from("Aiden Pearce"),
        age: 32,
        sex: 'M',
        job: String::from("Network security engineer"),
        income: 60000,
    };

    let mut profile_list = vec![aiden,];

    let mut new_profile_name = "name";
    let mut new_profile_age: u8 = 0;
    let mut new_profile_sex: char = '_';
    let mut new_profile_job = "job";
    let mut new_profile_income: u64 = 0;


    //Begin the main program
    loop{
        print!("{}[2J", 27 as char); //Clear terminal

        println!("
    ██████╗ ██████╗  ██████╗ ███████╗██╗██╗     ███████╗        ██████╗ ██████╗
    ██╔══██╗██╔══██╗██╔═══██╗██╔════╝██║██║     ██╔════╝        ██╔══██╗██╔══██╗
    ██████╔╝██████╔╝██║   ██║█████╗  ██║██║     █████╗          ██║  ██║██████╔╝
    ██╔═══╝ ██╔══██╗██║   ██║██╔══╝  ██║██║     ██╔══╝          ██║  ██║██╔══██╗
    ██║     ██║  ██║╚██████╔╝██║     ██║███████╗███████╗        ██████╔╝██████╔╝
    ╚═╝     ╚═╝  ╚═╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝╚══════╝        ╚═════╝ ╚═════╝
    By Ravenpaws
                ");
        println!("-----MAIN MENU-----");
        println!("VIEW PROFILES (v)");
        println!("ADD PROFILE (a)");
        println!("DELETE PROFILE (d)");
        println!("EXIT (e)");

        let mut input = get_input();

        if input == "v" {
            println!("Profile viewer screen.");

            input = get_input(); //Wait for keypress
        }

        if input == "a" {
            println!("Add a profile.");

            //Get name
            print!("Name: ");
            io::stdout().flush();
            let mut new_profile_name = get_input();

            //Get age
            print!("Age: ");
            io::stdout().flush();
            let mut new_profile_age = get_input().parse::<u8>().unwrap();

            //Get sex
            print!("Sex: ");
            io::stdout().flush();
            let mut new_profile_sex = get_input().parse::<char>().unwrap();

            //Get job
            print!("Job: ");
            io::stdout().flush();
            let mut new_profile_job = get_input();

            //Get income
            print!("Income: ");
            io::stdout().flush();
            let mut new_profile_income = get_input().parse::<u16>.unwrap().parse::<u64>.unwrap();

            let mut new_profile = Profile {
                name: new_profile_name;
                age: new_profile_age;
                sex = new_profile_sex;
                job = new_profile_job;
                income = new_profile_income;
            }

            profile_list.push(new_profile);

        } //Profile addition end

        if input == "d" {
            println!("Profile deletion screen.");

            input = get_input(); //Wait for keypress
        }

        if input == "e" {
            std::process::exit(0);
        }
    }

    //println!("{}, {}, {}", aiden.name, aiden.sex, aiden.age);
    //println!("{}, makes {}", aiden.job, aiden.income);
    /*println!("Name: {}", aiden.name);
    println!("Age: {}", aiden.age);
    println!("Sex: {}", aiden.sex);
    println!("Job: {}", aiden.job);
    println!("Income: {}", aiden.income);*/
}
